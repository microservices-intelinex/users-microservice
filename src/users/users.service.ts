import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User, UserDocument } from './entities/user.entity';

import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Pagination } from 'src/common/interfaces/pagination.interface';
import { buildPagination } from 'src/common/pagination';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {} // meter kafka
  create(createUserDto: CreateUserDto) {
    return this.userModel.create(createUserDto);
  }
  async findByEmail(email: string) {
    return this.userModel.findOne({ email });
  }
  async findByIdentification(identification: string) {
    return this.userModel.findOne({ identification });
  }
  async findAll(pagination: Pagination) {
    const total = await this.userModel.count();
    const currentPage = (pagination.page - 1) * pagination.limit;
    const data = await this.userModel
      .find()
      .select('-password')
      .limit(pagination.limit)
      .skip(currentPage);
    return buildPagination(
      data,
      pagination.page,
      pagination.limit,
      total,
      currentPage,
    );
  }
}
