import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { User, UserSchema } from './entities/user.entity';
import { MongooseModule } from '@nestjs/mongoose';

import { KAFKA_ID, MICROSERVICE_CONFIG } from '../config/constants';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';

export const MicroserviceConfig = ClientsModule.registerAsync([
  {
    name: KAFKA_ID,
    inject: [ConfigService],
    useFactory: (configService: ConfigService): any => {
      return {
        name: KAFKA_ID,
        transport: Transport.KAFKA,
        options: {
          client: configService.get(MICROSERVICE_CONFIG),
        },
      };
    },
  },
]);

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: User.name,
        schema: UserSchema,
      },
    ]),
    MicroserviceConfig,
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
