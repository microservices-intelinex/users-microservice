import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
  MinLength,
  IsNumber,
} from 'class-validator';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop({
    unique: true,
  })
  identification: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @Prop()
  location: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @Prop()
  age: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  @Prop()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @Prop()
  password: string;

  @Prop({
    default: new Date(),
  })
  createdAt?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
