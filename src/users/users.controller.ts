import { Controller, Get, Inject, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { KAFKA_ID } from 'src/config/constants';
import { ClientProxy } from '@nestjs/microservices';

import { PaginationPipe } from 'src/common/pipes/pagination.pipe';
import { Pagination } from 'src/common/interfaces/pagination.interface';
import {
  ApiQueryLimit,
  ApiQueryPage,
} from 'src/common/decorators/api-query.pagination';
import { ApiOperation } from '@nestjs/swagger';
@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    @Inject(KAFKA_ID) private readonly kafka: ClientProxy,
  ) {}
  @ApiOperation({
    summary: 'This endpoint allow query all the users',
  })
  @ApiQueryLimit()
  @ApiQueryPage()
  @Get()
  findAll(@Query(PaginationPipe) pagination: Pagination) {
    return this.usersService.findAll(pagination);
  }
}
