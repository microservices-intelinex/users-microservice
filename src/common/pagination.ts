export function buildPagination(results, currentPage, limit, total, page) {
  return {
    results,
    limit,
    total,
    currentPage,
    totalPages: Math.round(total / limit),
    hasNext: total / limit > currentPage,
  };
}
