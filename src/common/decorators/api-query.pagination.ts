import { ApiQuery } from '@nestjs/swagger';

export const ApiQueryLimit = () =>
  ApiQuery({
    required: false,
    name: 'limit',
  });
export const ApiQueryPage = () =>
  ApiQuery({
    required: false,
    name: 'page',
  });
export const ApiQueryISBN = () =>
  ApiQuery({
    required: false,
    name: 'ISBN',
  });

export const ApiQueryUserId = () =>
  ApiQuery({
    required: false,
    name: 'userId',
  });
