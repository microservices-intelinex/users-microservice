import {
  BadRequestException,
  Body,
  Controller,
  Logger,
  Post,
  Req,
  UnauthorizedException,
  Inject,
} from '@nestjs/common';
import { Request } from 'express';
import { UsersService } from 'src/users/users.service';
import { Login } from './dto/login.dto';
import * as jwt from 'jsonwebtoken';
import * as bcrypt from 'bcrypt';
import { KAFKA_ID } from 'src/config/constants';
import { ClientProxy } from '@nestjs/microservices';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { USER_CREATED } from 'src/config/events';
import { ApiOperation } from '@nestjs/swagger';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UsersService,
    @Inject(KAFKA_ID) private readonly kafka: ClientProxy,
  ) {}
  @ApiOperation({
    summary: 'This endpoint allow create a new user',
  })
  @Post('register')
  async create(@Body() createUserDto: CreateUserDto) {
    if (
      await this.userService.findByIdentification(createUserDto.identification)
    ) {
      throw new BadRequestException('The identification is aready registered');
    }
    if (await this.userService.findByEmail(createUserDto.email)) {
      throw new BadRequestException('The email is aready registered');
    }
    createUserDto.password = await bcrypt.hash(createUserDto.password, 10);
    const result = await this.userService.create(createUserDto);
    this.kafka.emit(USER_CREATED, {
      email: result.email,
      channel: 'EMAIL',
      type: 'WELCOME',
      name: `Dear ${result.email?.split('@')[0] || 'Joeh'}`,
    });
    return result;
  }

  @Post('login')
  async login(@Body() loginPayload: Login) {
    const user = await this.userService.findByEmail(loginPayload.email);
    if (
      !user ||
      !(await bcrypt.compare(loginPayload.password, user?.password))
    ) {
      throw new BadRequestException('bad credentials');
    }
    const payload = {
      sub: user.identification,
      email: user.email,
    };
    return {
      access_token: await jwt.sign(payload, process.env.JWT_SECRET_KEY, {
        expiresIn: process.env.JWT_SECRET_TIME_EXPIRE_HOURS?.trim() + 'h',
      }),
    };
  }
  @ApiOperation({
    summary: 'This endpoint check if the token bearer is valid or not',
  })
  @Post('access')
  async access(@Req() req: Request) {
    const bearer = req.headers['authorization'] as string;
    if (!bearer) {
      throw new UnauthorizedException('You need send a JWT token to access');
    }
    const token = bearer?.split(' ')[1];
    if (!token) {
      throw new UnauthorizedException(
        'You need send a JWT token to access format Bearer {TOKEN}',
      );
    }
    try {
      await jwt.verify(token, process.env.JWT_SECRET_KEY);
      return {
        message: 'Everything ok',
      };
    } catch (err) {
      Logger.error(err);
      throw new UnauthorizedException('Incorrect token');
    }
  }
}
