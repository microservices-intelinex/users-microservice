import { Module } from '@nestjs/common';
import { MicroserviceConfig, UsersModule } from 'src/users/users.module';
import { AuthController } from './auth.controller';

@Module({
  imports: [UsersModule, MicroserviceConfig],
  controllers: [AuthController],
  providers: [],
})
export class AuthModule {}
